package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MoviesListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    private MoviesListService moviesListService;

    @GetMapping(value = "/movies")
    public ResponseEntity<MovieListDto> getMovies() {

        return new ResponseEntity<>(moviesListService.readSortedMovies(),HttpStatus.OK);
    }

    @PutMapping(value = "/movies/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable int id ,@RequestBody CreateMovieDto updatedMovieDto) {

        return moviesListService.updateMovieMethod(updatedMovieDto,id) ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto newMovie) {

        return moviesListService.createMovieMethod(newMovie) ?  new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping(value = "/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable int id ) {

        return moviesListService.deleteMovieMethod(id) ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
