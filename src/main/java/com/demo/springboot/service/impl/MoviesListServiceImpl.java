package com.demo.springboot.service.impl;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MoviesListService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MoviesListServiceImpl implements MoviesListService {

    private final MovieListDto movies;
    private Integer lastFreeIndex;

    public MoviesListServiceImpl() {
        lastFreeIndex=1;
        List<MovieDto> moviesList = new ArrayList<>();
        moviesList.add(new MovieDto(lastFreeIndex++,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        movies = new MovieListDto(moviesList);
    }

    @Override
    public MovieListDto readSortedMovies() {
        Collections.sort(movies.getMovies(), Comparator.comparingInt(MovieDto::getMovieId));
        Collections.reverse(movies.getMovies());
        return movies;
    }

    @Override
    public boolean createMovieMethod( CreateMovieDto newMovie) {
        if(newMovie.getImage().isBlank() || newMovie.getTitle().isBlank() || newMovie.getYear() == null){
            return false;
        }
        movies.getMovies().add(new MovieDto(lastFreeIndex++,newMovie.getTitle(),newMovie.getYear(),newMovie.getImage()));
        return true;
    }

    @Override
    public boolean deleteMovieMethod(int id) {
        ListIterator<MovieDto> filmIterator = movies.getMovies().listIterator();
        while(filmIterator.hasNext()){
            if(filmIterator.next().getMovieId() == id){
                filmIterator.previous();
                filmIterator.remove();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean updateMovieMethod(CreateMovieDto updatedMovie, int id) {
        ListIterator<MovieDto> filmIterator = movies.getMovies().listIterator();
        while(filmIterator.hasNext()){
            if(filmIterator.next().getMovieId() == id){
                MovieDto tempFilm = filmIterator.previous();
                if(updatedMovie.getTitle()!=null){ tempFilm.setTitle(updatedMovie.getTitle()); }
                if(updatedMovie.getImage()!=null){ tempFilm.setImage(updatedMovie.getImage()); }
                if(updatedMovie.getYear()!=null){ tempFilm.setYear(updatedMovie.getYear()); }
                filmIterator.set(tempFilm);
                return true;
            }
        }
        return false;
    }
}
