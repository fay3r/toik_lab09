package com.demo.springboot.service;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.springframework.stereotype.Service;

@Service
public interface MoviesListService {
    MovieListDto readSortedMovies();
    boolean createMovieMethod(CreateMovieDto newMovie);
    boolean deleteMovieMethod(int id);
    boolean updateMovieMethod(CreateMovieDto updatedMovie,int id);
}
